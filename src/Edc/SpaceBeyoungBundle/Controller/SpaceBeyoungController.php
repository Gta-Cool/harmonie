<?php

namespace Edc\SpaceBeyoungBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SpaceBeyoungController extends Controller
{
    /**
     * @Route("/{_locale}/", name="space_beyoung", requirements={"_locale"="fr|en"})
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }
}
