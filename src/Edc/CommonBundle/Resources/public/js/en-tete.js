/* @ Gta-Cool pour l'écho de charbonnières */

var largeur_banniere = 500 + 10; // + margin sur les cotés
var largeur_vignette = 150 + 10; // + margin sur les cotés

var vitesse_changement = 1000 * 4; // car millisecondes doit être supérieur à "vitesse_disparition + vitesse_apparition"
var vitesse_disparition = 1000 * 1;
var vitesse_apparition = 1000 * 2;

var largeur_zone_banniere = 0;
var nbr_vignettes = 0;
var nbr_vignettes_cote = 0;
var nbr_vignettes_cote_visible = 0;
var cote_aleat = true; // Coté commençant, true -> droite, false -> gauche

var numeros_vignettes = new Array();
var numeros_positions = new Array();

var defaultImageUrl = '/bundles/edccommon/images';

var ie = getInternetExplorerVersion();
var ie8 = false;
if ( ie === 8 ) {
    ie8 = true;
}


function aleat_vignette(position) { // retourne un entier
    var aleat = Math.floor(Math.random() * (vignette_max));
    var present = false;
    for(var j =0; j < numeros_positions.length; j++) {
        if(numeros_vignettes[numeros_positions[j]] === aleat) {
            present = true;
            break;
        }
    }
    if(present) return aleat_vignette(position);
    else {
        numeros_positions[numeros_positions.length] = position;
        numeros_vignettes[position] = aleat;
        return aleat;
    }
}

function change_position() {
    if((largeur_page - largeur_zone_banniere) > 0) {
        $('#principal #en-tete .zone-banniere div.vignette:first-child').before('<div class="vignette before ' + nbr_vignettes_cote + '" ><img src="' + defaultImageUrl + '/general/vignette.gif" /></div>');
        charger_image(url_vignettes[aleat_vignette('#principal #en-tete .zone-banniere .vignette.before.' + nbr_vignettes_cote)], '#principal #en-tete .zone-banniere .vignette.before.' + nbr_vignettes_cote);
        $('#principal #en-tete .zone-banniere div.vignette:last-child').after('<div class="vignette after ' + nbr_vignettes_cote + '" ><img src="' + defaultImageUrl + '/general/vignette.gif" /></div>');
        charger_image(url_vignettes[aleat_vignette('#principal #en-tete .zone-banniere .vignette.after.' + nbr_vignettes_cote)], '#principal #en-tete .zone-banniere .vignette.after.' + nbr_vignettes_cote);
        nbr_vignettes_cote++;
        largeur_zone_banniere += largeur_vignette * 2;
        $('#principal #en-tete .zone-banniere').css('width', largeur_zone_banniere + 'px');
        change_position();
    }
    else
    {
        nbr_vignettes_cote_visible = parseInt(((largeur_page - largeur_banniere) / largeur_vignette) / 2) + 1
        var decalage = (largeur_zone_banniere - largeur_page) / 2;
        $('#principal #en-tete .zone-banniere').css('left', '-' + decalage + 'px');
        $('#principal #en-tete').css('width', largeur_page + 'px');
    }
}

function charger_image(imageSrc, position) {
    var img = new Image();
    img.src = imageSrc;
    img.onload = function() {
        intervertir(imageSrc, position);
    };
}

function intervertir(imageSrc, position) {
    $(position + ' img').addClass('ancienne');
    $(position + ' img').after('<img src="' + imageSrc + '" class="nouvelle" />');
    $(position + ' img.nouvelle').css('display', 'none');

    $(position + ' img.ancienne').fadeOut(vitesse_disparition);
    $(position + ' img.nouvelle').fadeIn(vitesse_apparition);

    $(position + ' img.ancienne').remove();
    $(position + ' img.nouvelle').removeClass('nouvelle');

    var nbr = 0;

    $(position + ' img').each(function(){
            nbr++;
    });
}

function changement_aleatoire() {
    var nbr_vig;
    if(nbr_vignettes_cote_visible !== 0)
            nbr_vig = nbr_vignettes_cote_visible;
    else
            nbr_vig = nbr_vignettes_cote;

    var aleat = Math.floor(Math.random() * (nbr_vig));
    var cote;
    if (cote_aleat) {
            cote = 'after';
            cote_aleat = false;
    }
    else {
            cote = 'before';
            cote_aleat = true;
    }

    charger_image(url_vignettes[aleat_vignette('#principal #en-tete .zone-banniere .vignette.' + cote + '.' + aleat)], '#principal #en-tete .zone-banniere .vignette.' + cote + '.' + aleat);
}

$(document).ready(function(){

    var espace_vignettes = largeur_page - largeur_banniere;

    if(espace_vignettes > 0) {
        nbr_vignettes_cote = parseInt(espace_vignettes / (largeur_vignette * 2));
        nbr_vignettes = nbr_vignettes_cote * 2;
        if((espace_vignettes - (nbr_vignettes * largeur_vignette)) > 10) {
            nbr_vignettes += 2;
            nbr_vignettes_cote++;
        }
    }

    $('#principal #en-tete .zone-banniere .banniere img').css('margin', '0 5px');

    largeur_zone_banniere = largeur_vignette * nbr_vignettes + largeur_banniere;

    if(ie8)
    {
        largeur_zone_banniere += 4; // Décalage pour ie8
    }

    //$('#principal #en-tete .zone-banniere').css('width', largeur_zone_banniere + 'px');
    $('#principal #en-tete .zone-banniere').width(largeur_zone_banniere);

    if(largeur_zone_banniere > largeur_page) {
        var decalage = (largeur_zone_banniere - largeur_page) / 2;
        $('#principal #en-tete .zone-banniere').css('position', 'absolute');
        $('#principal #en-tete .zone-banniere').css('left', '-' + decalage + 'px');
    }

    for(var i=(nbr_vignettes_cote - 1); i >= 0; i--) {
        $('#principal #en-tete .zone-banniere .banniere').before('<div class="vignette before ' + i + '" ><img src="' + defaultImageUrl + '/general/vignette.gif" /></div>');
        charger_image(url_vignettes[aleat_vignette('#principal #en-tete .zone-banniere .vignette.before.' + i)], '#principal #en-tete .zone-banniere .vignette.before.' + i);
        $('#principal #en-tete .zone-banniere .banniere').after('<div class="vignette after ' + i + '" ><img src="' + defaultImageUrl + '/general/vignette.gif" /></div>');
        charger_image(url_vignettes[aleat_vignette('#principal #en-tete .zone-banniere .vignette.after.' + i)], '#principal #en-tete .zone-banniere .vignette.after.' + i);
    }

    $('#principal #en-tete .zone-banniere .banniere').css('display', 'inline-block');

    $(window).resize(function(){
        change_position();
    });

    $(window).bind('fullscreen-on', function(e) {
        change_position();
    });

    setInterval('changement_aleatoire()', vitesse_changement);

    $('#bandeau ul.menu li.active').each(function(){
        $(this).append('<div class="triangle"></div>');
    });

    $('#principal #en-tete .menu-principal ul.menu li.parent').hover(function(){
        $(this).find('ul').slideDown('fast');
    },function(){
        $(this).find('ul').stop().slideUp('fast');
    });
});
