
var largeur_page = 0;
var hauteur_page = 0;

largeur_page = $(window).width();
hauteur_page = $(window).height();

$(window).resize(function(){
    largeur_page = $(window).width();
    hauteur_page = $(window).height();
});

/*  Usage:
    var path = $('#foo').getPath();
*/
(function($){
    $.fn.extend({
        getIdentifiant: function() {
            // The first time this function is called, path won't be defined.
            if ( typeof path === 'undefined' ) {
                path = '';
            }

            var cur = '';

            // Determine the IDs and path.
            var id = this.attr('id');
            var	classe = this.attr('class');

            // Add the #id if there is one.
            if ( typeof id !== 'undefined' ) {
                cur += '#' + id;
            }

            // Add any classes.
            if ( typeof classe !== 'undefined' ) {
                cur += '.' + classe.split(/[\s\n]+/).join('.');
            }

            return cur;
        }
    });

    $.fn.extend({
        getPath: function( path ) {
            // The first time this function is called, path won't be defined.
            if ( typeof path === 'undefined' ) {
                path = '';
            }

            // If this element is <html> we've reached the end of the path.
            if ( this.is('html') ) {
                return 'html' + path;
            }

            // Add the element name.
            var cur = this.get(0).nodeName.toLowerCase();

            // Determine the IDs and path.
            var id = this.attr('id');
            var	classe = this.attr('class');


            // Add the #id if there is one.
            if ( typeof id !== 'undefined' ) {
                cur += '#' + id;
            }

            // Add any classes.
            if ( typeof classe !== 'undefined' ) {
                cur += '.' + classe.split(/[\s\n]+/).join('.');
            }

            // Recurse up the DOM.
            return this.parent().getPath( ' > ' + cur + path );
        }
    });

})($);

// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
function getInternetExplorerVersion()
{
    var rv = -1; // Return value assumes failure.
    if (navigator.appName === 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) !== null)
        rv = parseFloat( RegExp.$1 );
    }
    return rv;
}


$(document).ready(function() {

    //var en_tete_et_page = $("#principal #en-tete").height() + $("#principal #page").height() + 20 /*padding hauteur de page*/;
    //var hauteur_totale_block = en_tete_et_page + $("#principal #pied-de-page").height();

    //if(hauteur_totale_block < hauteur_page)
    //{
    //	$("#principal #pied-de-page").height(hauteur_page - en_tete_et_page);
    //}

});


