<?php

namespace Edc\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class LayoutController extends Controller
{
    /**
     * @Template()
     */
    public function headerAction()
    {
        $urlsVignette = array();
        $lastVignette = 0;
        $hackAdmin = '23.jpg'; // Hack privil�ge de l'admin :D

        $dirPathVignette = '/bundles/edccommon/images/portraits/';
        $dirVignettes = $this->getRequest()->server->get('DOCUMENT_ROOT').$dirPathVignette;

        if(\is_dir($dirVignettes)) {
            $dir = \opendir($dirVignettes);
            if($dir) {
                $i = 0;
                while($element = readdir($dir)) {
                    if($element != '.' && $element != '..' && \getimagesize($dirVignettes . $element) && $element != $hackAdmin) {
                        $urlsVignette[$i] = $dirPathVignette . $element;
                        $i++;
                    }
                }
                $lastVignette = $i;
            }
        }

        return array(
            'urlsVignette' => $urlsVignette,
            'lastVignette' => $lastVignette
        );
    }

    /**
     * @Template()
     */
    public function footerAction()
    {
        return array();
    }
}
