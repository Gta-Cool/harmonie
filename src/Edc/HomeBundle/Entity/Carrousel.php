<?php

namespace Edc\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrousel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Carrousel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255)
     */
    private $lien;

    /**
     * @var integer
     *
     * @ORM\Column(name="tri", type="integer")
     */
    private $tri;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean")
     */
    private $actif;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Carrousel
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set lien
     *
     * @param string $lien
     * @return Carrousel
     */
    public function setLien($lien)
    {
        $this->lien = $lien;
    
        return $this;
    }

    /**
     * Get lien
     *
     * @return string 
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set tri
     *
     * @param integer $tri
     * @return Carrousel
     */
    public function setTri($tri)
    {
        $this->tri = $tri;
    
        return $this;
    }

    /**
     * Get tri
     *
     * @return integer 
     */
    public function getTri()
    {
        return $this->tri;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Carrousel
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    
        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }
}
