<?php

namespace Edc\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeController extends Controller
{
    /**
     * @Route("/{_locale}/", name="home", requirements={"_locale"="fr|en"})
     * @Route("/", name="racine")
     * @Template()
     */
    public function indexAction($_locale = null)
    {
        // Si la locale n'est pas mention�e dans la route
        if ($_locale == null) {

            // Si la locale existe dans la session
            if ($this->getRequest()->getSession()->has('locale')) {
                // On force la locale de la request avec celle de la session
                $this->getRequest()->setLocale($this->getRequest()->getSession()->get('locale'));
            } else {
                // Sinon on place la locale pr�f�r�e dans la request
                $this->getRequest()->setLocale($this->getRequest()->getPreferredLanguage(array('en', 'fr')));
            }

            // Redirection (302) vers la homepage avec la locale
            return $this->redirect($this->generateUrl('home', array('_locale' => $this->getRequest()->getLocale())), 302);
        }

        // On stock la locale dans la session
        $this->getRequest()->getSession()->set('locale', $this->getRequest()->getLocale());

        return array('name' => 'visiteur');
    }
}
